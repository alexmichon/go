"""
Module for playing games of Go using GoTextProtocol

This code is based off of the gtp module in the Deep-Go project
by Isaac Henrion and Amos Storkey at the University of Edinburgh.
"""
import traceback
import sys
import os
from board import GoBoard
from board_util import GoBoardUtil, BLACK, WHITE, EMPTY, BORDER, FLOODFILL
import numpy as np
import re
import random
from timeit import default_timer

class GtpConnection():

    def __init__(self, go_engine, debug_mode = False):
        """
        object that plays Go using GTP

        Parameters
        ----------
        go_engine: GoPlayer
            a program that is capable of playing go by reading GTP commands
        debug_mode: prints debug messages
        """
        self.startTimer = 0
        self.winMoves = []
        self.stdout = sys.stdout
        sys.stdout = self
        self._debug_mode = debug_mode
        self.go_engine = go_engine
        self.komi = 0
        self.board = GoBoard(7)
        self.seconds = 1
        self.solving = True
        self.commands = {
            "protocol_version": self.protocol_version_cmd,
            "quit": self.quit_cmd,
            "name": self.name_cmd,
            "boardsize": self.boardsize_cmd,
            "showboard": self.showboard_cmd,
            "clear_board": self.clear_board_cmd,
            "komi": self.komi_cmd,
            "version": self.version_cmd,
            "known_command": self.known_command_cmd,
            "set_free_handicap": self.set_free_handicap,
            "genmove": self.genmove_cmd,
            "list_commands": self.list_commands_cmd,
            "play": self.play_cmd,
            "final_score": self.final_score_cmd,
            "legal_moves": self.legal_moves_cmd,
            "timelimit": self.time_limit_cmd,
            "solve": self.solve_cmd
        }

        # used for argument checking
        # values: (required number or arguments, error message on argnum failure)
        self.argmap = {
            "boardsize": (1, 'Usage: boardsize INT'),
            "komi": (1, 'Usage: komi FLOAT'),
            "known_command": (1, 'Usage: known_command CMD_NAME'),
            "set_free_handicap": (1, 'Usage: set_free_handicap MOVE (e.g. A4)'),
            "genmove": (1, 'Usage: genmove {w, b}'),
            "play": (2, 'Usage: play {b, w} MOVE'),
            "legal_moves": (1, 'Usage: legal_moves {w, b}'),
            "timelimit": (1, 'Usage: timelimit (1 >= SECONDS <= 100)')
        }
    
    def __del__(self):
        sys.stdout = self.stdout

    def write(self, data):
        self.stdout.write(data)

    def flush(self):
        self.stdout.flush()

    def start_connection(self):
        """
        start a GTP connection. This function is what continuously monitors
        the user's input of commands.
        """
        self.debug_msg("Start up successful...\n\n")
        line = sys.stdin.readline()
        while line:
            self.get_cmd(line)
            line = sys.stdin.readline()

#------------------------------------------------------------------------



    def time_limit_cmd(self, command):
        
        #commands = command.strip()
        self.seconds = int(command[0])
        if self.seconds < 1 or self.seconds > 100:
            self.respond("seconds must be between 1 and 100")
        else:
            self.respond()


#------------------------------------------------------------------------


    def get_cmd(self, command):
        """
        parse the command and execute it

        Arguments
        ---------
        command : str
            the raw command to parse/execute
        """
        if len(command.strip(' \r\t')) == 0:
            return
        if command[0] == '#':
            return
        # Strip leading numbers from regression tests
        if command[0].isdigit():
            command = re.sub("^\d+", "", command).lstrip()

        elements = command.split()
        if not elements:
            return
        command_name = elements[0]; args = elements[1:]
        
        if command_name == "play" and self.argmap[command_name][0] != len(args):
            self.respond('illegal move: {} wrong number of arguments'.format(args[0]))
            return

        if self.arg_error(command_name, len(args)):
            return
        if command_name in self.commands:
            try:
                self.commands[command_name](args)
            except Exception as e:
                self.debug_msg("Error executing command {}\n".format(str(e)))
                self.debug_msg("Stack Trace:\n{}\n".format(traceback.format_exc()))
                raise e
        else:
            self.debug_msg("Unknown command: {}\n".format(command_name))
            self.error('Unknown command')
            sys.stdout.flush()

    def arg_error(self, cmd, argnum):
        """
        checker funciton for the number of arguments given to a command

        Arguments
        ---------
        cmd : str
            the command name
        argnum : int
            number of parsed argument

        Returns
        -------
        True if there was an argument error
        False otherwise
        """
        if cmd in self.argmap and self.argmap[cmd][0] > argnum:
            self.error(self.argmap[cmd][1])
            return True
        return False

    def debug_msg(self, msg = ''):
        """ Write a msg to the debug stream """
        if self._debug_mode:
            sys.stderr.write(msg); sys.stderr.flush()

    def error(self, error_msg = ''):
        """ Send error msg to stdout and through the GTP connection. """
        sys.stdout.write('? {}\n\n'.format(error_msg)); sys.stdout.flush()

    def respond(self, response = ''):
        """ Send msg to stdout """
        sys.stdout.write('= {}\n\n'.format(response)); sys.stdout.flush()

    def reset(self, size):
        """
        Resets the state of the GTP to a starting board

        Arguments
        ---------
        size : int
            the boardsize to reinitialize the state to
        """
        self.board.reset(size)

    def protocol_version_cmd(self, args):
        """ Return the GTP protocol version being used (always 2) """
        self.respond('2')

    def quit_cmd(self, args):
        """ Quit game and exit the GTP interface """
        self.respond()
        exit()

    def name_cmd(self, args):
        """ Return the name of the player """
        self.respond(self.go_engine.name)

    def version_cmd(self, args):
        """ Return the version of the player """
        self.respond(self.go_engine.version)

    def clear_board_cmd(self, args):
        """ clear the board """
        self.reset(self.board.size)
        self.respond()

    def boardsize_cmd(self, args):
        """
        Reset the game and initialize with a new boardsize

        Arguments
        ---------
        args[0] : int
            size of reinitialized board
        """
        self.reset(int(args[0]))
        self.respond()

    def showboard_cmd(self, args):
        self.respond('\n' + str(self.board.get_twoD_board()))

    def komi_cmd(self, args):
        """
        Set the komi for the game

        Arguments
        ---------
        args[0] : float
            komi value
        """
        self.komi = float(args[0])
        self.respond()

    def known_command_cmd(self, args):
        """
        Check if a command is known to the GTP interface

        Arguments
        ---------
        args[0] : str
            the command name to check for
        """
        if args[0] in self.commands:
            self.respond("true")
        else:
            self.respond("false")

    def list_commands_cmd(self, args):
        """ list all supported GTP commands """
        self.respond(' '.join(list(self.commands.keys())))

    def set_free_handicap(self, args):
        """
        clear the board and set free handicap for the game

        Arguments
        ---------
        args[0] : str
            the move to handicap (e.g. B2)
        """
        self.board.reset(self.board.size)
        for point in args:
            move = GoBoardUtil.move_to_coord(point, self.board.size)
            point = self.board._coord_to_point(*move)
            if not self.board.move(point, BLACK):
                self.debug_msg("Illegal Move: {}\nBoard:\n{}\n".format(move, str(self.board.get_twoD_board())))
        self.respond()

    def legal_moves_cmd(self, args):
        """
        list legal moves for the given color
        Arguments
        ---------
        args[0] : {'b','w'}
            the color to play the move as
            it gets converted to  Black --> 1 White --> 2
            color : {0,1}
            board_color : {'b','w'}
        """
        try:
            board_color = args[0].lower()
            color = GoBoardUtil.color_to_int(board_color)
            moves = GoBoardUtil.generate_legal_moves(self.board, color)
            self.respond(moves)
        except Exception as e:
            self.respond('Error: {}'.format(str(e)))

    def play_cmd(self, args):
        """
        play a move as the given color

        Arguments
        ---------
        args[0] : {'b','w'}
            the color to play the move as
            it gets converted to  Black --> 1 White --> 2
            color : {0,1}
            board_color : {'b','w'}
        args[1] : str
            the move to play (e.g. A5)
        """
        try:
            board_color = args[0].lower() #eg b
            board_move = args[1]          #eg a1
            color = GoBoardUtil.color_to_int(board_color)
            move = GoBoardUtil.move_to_coord(args[1], self.board.size)
            if move:
                move = self.board._coord_to_point(move[0], move[1]) #if move existst 
            else:
                return
            if not self.board.move(move, color):
                return
            self.respond()
        except Exception as e:
            self.respond("illegal move: {} {} {}".format(board_color, board_move, str(e)))
#--------------------------------------------------------------------------------------------------
    #def play_copy(self, board_color, board_move):
    def play_copy(board_color, board_move):#same as play_cmd except goes through copied board
        try:
            color = GoBoardUtil.color_to_int(board_color)
            move = GoBoardUtil.move_to_coord(args[1], self.tempBoard.size)
            if move:
                move = self.tempBoard._coord_to_point(move[0], move[1])
            else:
                return
            if not self.tempBoard.move(move, color):
                return
            self.respond() #remove l8er
        except Exception as e:
            self.respond("illegal move: {} {} {}".format(board_color, board_move, str(e)))
#--------------------------------------------------------------------------------------------------
    def final_score_cmd(self, args):
        self.respond(self.board.final_score(self.komi))

    def genmove_cmd(self, args):
        """
        generate a move for the specified color

        Arguments
        ---------
        args[0] : {'b','w'}
            the color to generate a move for
            it gets converted to  Black --> 1 White --> 2
            color : {0,1}
            board_color : {'b','w'}
        """
        '''try:
            board_color = args[0].lower()
            color = GoBoardUtil.color_to_int(board_color)
            move = self.go_engine.get_move(self.board, color)
            if move is None:
                self.respond("pass")
                return

            if not self.board.check_legal(move, color):
                move = self.board._point_to_coord(move)
                board_move = GoBoardUtil.format_point(move)
                self.respond("Illegal move: {}".format(board_move))
                raise RuntimeError("Illegal move given by engine")

            # move is legal; play it
            self.board.move(move, color)
            self.debug_msg("Move: {}\nBoard: \n{}\n".format(move, str(self.board.get_twoD_board())))
            move = self.board._point_to_coord(move)
            board_move = GoBoardUtil.format_point(move)
            self.respond(board_move)
        except Exception as e:
            self.respond('Error: {}'.format(str(e)))'''

        #dont print solve statements
        self.solving = False
        try:
            #get color and playable moves
            color= GoBoardUtil.color_to_int(args[0].lower())
            playable_moves = GoBoardUtil.generate_legal_moves(self.board,color)
            playable = playable_moves.split()
            
            #move = self.board._point_to_coord(move)
            '''board_move =  GoBoardUtil.format_point(int(move))
                self.respond(type(board_move))
                self.board._play_move(board_move,color)'''
            if len(playable) == 0:
                self.respond("resign")
            else:
                #if no solve or run out of time, play random
                winning = self.solve_cmd("solve")
                if (self.outOfTime == True):
                    move = (playable)[random.randint(0,len(playable)-1)]
                elif (len(winning)==0):
                    move = (playable)[random.randint(0,len(playable)-1)]
                else:
                    move = winning
                #self.get_cmd("play {} {}".format(args[0].lower(), move))       Dont play move
                self.respond("{} {}".format(args[0].lower(), move))
        
        #handle wrong input
        except Exception as e:
            self.respond('Error: {}'.format(str(e)))
        self.solving = True

#---------------------------------------------------------------------------------------


    def solve_cmd(self,args):
        self.startTimer = default_timer() #set startTimer to wall clock
        self.tempBoard = self.board.copy() #creats copy of board in new tempBoard object
        self.depth = 0
        
        color = self.board.to_play
        winner = self.MinimaxBooleanOR(color) #returns None if time runs out, True if player can win, False if cannot
        if winner:
            if color == 1:
                prntColor = 'b'
            else:
                prntColor = 'w'
            winLength=0
            
            #if solving, print
            if self.solving == True:
                self.respond("{} {}".format(prntColor,self.bestMove))
            #else return values
            else:
                self.outOfTime = False
                return self.bestMove
    
        elif winner == False:
            if color == 1:
                prntColor = 'w'
            else:
                prntColor = 'b'
            if self.solving == True:
                self.respond("{}".format(prntColor))
            self.outOfTime = True
        else:
            if self.solving == True:
                prntColor = 'unknown'
                self.respond("{}".format(prntColor))
            self.outOfTime = True
        
        
        



    def MinimaxBooleanOR(self,color): #is Tree and not DAG
        if (default_timer() - self.startTimer) > self.seconds: #if over timer, leave loop
            return None
        playable = GoBoardUtil.generate_legal_moves(self.tempBoard,color).split()
        
        for move in playable:
            if self.depth == 0:
                self.bestMove = move
            
            playCoord = GoBoardUtil.move_to_coord(move, self.tempBoard.size) #convers point to (row, col)
            playCoord = self.tempBoard._coord_to_point(playCoord[0], playCoord[1]) #check puys point through board
            self.depth = self.depth + 1         #<-
            self.tempBoard.move(playCoord, color)
            isWin = self.MinimaxBooleanAND(3-color)#3-colour = opponent colour
            self.tempBoard._undo_move(playCoord)
            self.depth = self.depth - 1         #<-
            if (isWin == None):
                return None
            if (isWin):
                return True
        return False


    def MinimaxBooleanAND(self,color):
        playable = GoBoardUtil.generate_legal_moves(self.tempBoard,color).split()
        for move in playable:
            playCoord = GoBoardUtil.move_to_coord(move, self.tempBoard.size) #convers point to (row, col)
            playCoord = self.tempBoard._coord_to_point(playCoord[0], playCoord[1]) #check puys point through board
            self.depth = self.depth + 1         #<-
            self.tempBoard.move(playCoord, color)
            isWin = self.MinimaxBooleanOR(3-color)
            self.tempBoard._undo_move(playCoord)
            self.depth = self.depth - 1         #<-
            if (isWin == None): #if over timer
                return None
            elif (isWin == False):
                return False
        return True
